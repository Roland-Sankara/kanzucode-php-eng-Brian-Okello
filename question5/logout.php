<?php 
    include "includes/deploy.php";
    session_destroy(); // Destroy the session

    // Redirect to the login page
    header('location: login.php');
    exit; 
?>
