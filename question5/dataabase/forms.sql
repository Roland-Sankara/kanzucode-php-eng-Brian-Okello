-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2023 at 06:46 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forms`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `staff` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `contact` varchar(256) NOT NULL,
  `gender` varchar(256) NOT NULL,
  `occupation` varchar(256) NOT NULL,
  `residence` varchar(256) NOT NULL,
  `members` int(11) NOT NULL,
  `male` int(11) NOT NULL,
  `female` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `user_id`, `staff`, `name`, `email`, `contact`, `gender`, `occupation`, `residence`, `members`, `male`, `female`, `created`) VALUES
(1, 0, 'Okello Brian', 'Okello Brian', 'okellobrian64@gmail.com', '0781033456', 'Male', 'Lawyer', 'Kampala', 8, 4, 4, '2023-08-21 13:26:04'),
(2, 0, 'Okello Brian', 'Kato Laban', 'laban@gmail.com', '0781038321', '', 'Lawyer', 'Kawempe', 9, 7, 2, '2023-08-21 13:26:16'),
(3, 0, 'Okello Brian', 'Allen Scola', 'scola@gmail.com', '0781033456', '', 'Marketer', 'Muyenga', 7, 4, 3, '2023-08-21 13:26:26'),
(4, 0, 'Okello Brian', 'Allen Nella', 'nella@gmail.com', '0781030001', '', 'Manager', '0', 6, 4, 2, '2023-08-21 13:25:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `contact` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `contact`, `password`, `role`, `created_at`) VALUES
(1, 'Okello Brian', 'okellobrian64@gmail.com', '0781038320', '$2y$10$YOyXT3onCA814l0Q4zhyCOFHX16LQqZtZidr8I9ZAh.sW3cHHF8ZC', 'Staff', '2023-08-20 18:42:41'),
(3, 'Omagor Shaban', 'shaban@gmail.com', '0777567345', '$2y$10$0I6DJAhGhDWdmL2REiYS.eGGvsDzHco6mhpR5hGwOIVG1Fk1BE5Ue', 'Staff', '2023-08-18 09:51:59'),
(4, 'Ajang Esther ', 'esther@gmail.com', '0781038325', '$2y$10$Tt9whcyMgf48ofp5osAuw.Olos93qrYzZAzrFdjbRvEwpi5Wsmd8C', 'Staff', '2023-08-18 15:58:46'),
(5, 'Admin', 'admin@gmail.com', '0775040210', '$2y$10$KvKOGB3.93cCNFBdO0a8L.ObVi9OWSCeNWlGGaH/I76r2Q8G6D0uS', 'Admin', '2023-08-18 17:35:58'),
(6, 'aaaaa', 'a@gmail.com', '077774567', '$2y$10$NNCOOJ6jAtrHiDWiUFoAx.Iup8yF46Cbq9oVKeF6hgGM3.d6b0tZG', 'Staff', '2023-08-18 17:46:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
