<?php
include "verify.php";
?>
<!DOCTYPE html>
<!--By okello Brian-->
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible", content="ie=edge">
    <title>Jaba Consultancy Company</title>
    <link rel="stylesheet" href="login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
</head>

<body style="background-color: #5E72E4;">
    <div class="wrapper">
        <section class="form login">
            <header style="text-align: center;">
                <p>JABA CONSULTANCY COMPANY</p>
            </header>
            <?php if (isset($_GET['error'])) { ?>
            <p style="background: #F2DEDE;
        color: #A94442;
        padding: 10px;
        width: 95%;
        border-radius: 5px;
        margin: 20px auto;">
                <?php echo $_GET['error']; ?> <a style="font-size:20px; margin-left: 120px;" href="login.php">&times</a></p>

            <?php } ?>
            <form action="" method="post">
                <!-- <div class="error-txt">This is an error message</div> -->


                <div class="name-details">

                    <div class="field input">
                        <label>Email Address <span style="color: #FF0000">*</span></label>
                        <input type="email" name="email" placeholder="Enter your Email Address">
                    </div>
                    <div class="field input">
                        <label>Password <span style="color: #FF0000">*</span></label>
                        <input type="password" name="password" autocomplete="current-password" class="fa fa-eye" required=""
                            id="id_password">
                        <i class="fa fa-eye" id="togglePassword"></i>
                    </div>
                    <div class="field button">
                        <input type="submit" name="Login" style="text-align:center" value="Login">
                    </div>
            </form>
        </section>
    </div>

    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#id_password');

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });
    </script>
</body>
</html>
