<?php
// Include the database connection
include "includes/deploy.php";

// Function to validate email format
function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

// Initialize variables for success and error messages
$success = "";
$error = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Input validation and data sanitization
    $staff = isset($_POST['staff']) ? trim($_POST['staff']) : '';
    $name = isset($_POST['name']) ? trim($_POST['name']) : '';
    $email = isset($_POST['email']) ? trim($_POST['email']) : '';
    $gender = isset($_POST['role']) ? $_POST['role'] : '';
    $contact = isset($_POST['contact']) ? substr(trim($_POST['contact']), 0, 10) : ''; // Limit to 10 characters
    $occupation = isset($_POST['occupation']) ? trim($_POST['occupation']) : '';
    $residence = isset($_POST['residence']) ? trim($_POST['residence']) : '';
    $members = isset($_POST['members']) ? intval($_POST['members']) : 0; // Assuming it's an integer
    $male = isset($_POST['male']) ? intval($_POST['male']) : 0; // Assuming it's an integer
    $female = isset($_POST['female']) ? intval($_POST['female']) : 0; // Assuming it's an integer

    // Validate email
    if (!isValidEmail($email)) {
        $error = "Invalid email format.";
    } else {
        // Check if email is already registered
        $emailCheckQuery = "SELECT * FROM data WHERE email = ?";
        $stmt = $connection->prepare($emailCheckQuery);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $error = "Email already registered.";
        } else {
            // Insert user data into the database
            $insertQuery = "INSERT INTO `data`(`staff`, `name`, `email`, `contact`, `gender`, `occupation`, `residence`, `members`, `male`, `female`)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $connection->prepare($insertQuery);
            $stmt->bind_param('ssssssiiii', $staff, $name, $email, $contact, $gender, $occupation, $residence, $members, $male, $female);

            if ($stmt->execute()) {
                $success = "Info added successfully";
            } else {
                $error = "Error: " . $stmt->error;
            }
        }
    }
}

// Close the database connection
$connection->close();

if (!empty($success)) {
    header("Location: examples/staff.php?success=" . urlencode($success));
    exit();
} elseif (!empty($error)) {
    header("Location: examples/staff.php?error=" . urlencode($error));
    exit();
}
?>
