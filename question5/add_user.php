<?php
include "includes/deploy.php";
// Function to validate email format
function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

$success = "";
$error = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $role = $_POST['role'];
    $contact = substr($_POST['contact'], 0, 10); // Limit to 10 characters
    $password = $_POST['password'];
    
    // Validate email
    if (!isValidEmail($email)) {
        $error = "Invalid email format.";
    } else {
        // Check if email is already registered
        $emailCheckQuery = "SELECT * FROM users WHERE email = '$email'";
        $emailCheckResult = $connection->query($emailCheckQuery);
        
        if ($emailCheckResult->num_rows > 0) {
            $error = "Email already exists.";
        } else {
            // Hash the password before storing
            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
            
            // Insert user data into database
            $insertQuery = "INSERT INTO `users`(`name`, `email`, `contact`, `password`, `role`) VALUES ('$name', '$email', '$contact', '$hashedPassword','$role')";
            
            if ($connection->query($insertQuery) === TRUE) {
                $success = "User registered successfully.";
            
            } else {
                $error = "Error: " . $connection->error;
            }
        }
    }
}

$connection->close();

if (!empty($success)) {
    header("Location: examples/user.php?success=" . urlencode($success));
    exit();
} elseif (!empty($error)) {
    header("Location: examples/user.php?error=" . urlencode($error));
    exit();
}
?>
