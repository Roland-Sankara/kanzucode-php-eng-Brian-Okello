<?php
// Establish database connection
$connection = mysqli_connect("hostname", "username", "password", "database");

if (!$connection) {
    die("Database connection failed: " . mysqli_connect_error());
}

// Deleting User
if (isset($_GET['delete'])) {
    $userId = $_GET['delete'];
    $sql_delete = "DELETE FROM users WHERE user_id = ?";
    $stmt = $connection->prepare($sql_delete);
    $stmt->bind_param("i", $userId);
    
    if ($stmt->execute()) {
        echo "Deleted successfully";
    } else {
        echo "Error: " . $stmt->error;
    }
    $stmt->close();
}

// Handling user updates
if (isset($_POST['edit_id'])) {
    $userId = $_POST['edit_id'];
    $newName = $_POST['name'];
    $newEmail = $_POST['email'];
    $newContact = $_POST['contact'];
    $newRole = $_POST['role'];

    $sql_update = "UPDATE users SET name = ?, email = ?, contact = ?, role = ? WHERE user_id = ?";
    $stmt = $connection->prepare($sql_update);
    $stmt->bind_param("ssssi", $newName, $newEmail, $newContact, $newRole, $userId);
    
    if ($stmt->execute()) {
        echo "Updated successfully";
    } else {
        echo "Error: " . $stmt->error;
    }
    $stmt->close();
}

// Retrieving data
$query = "SELECT * FROM users";
$result = $connection->query($query);
?>

<!DOCTYPE html>
<html>
<head>
    <!-- Include necessary CSS and JS files -->
</head>
<body>
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Users</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="sort" data-sort="name">Name</th>
                                    <th scope="col" class="sort" data-sort="budget">Email</th>
                                    <th scope="col" class="sort" data-sort="status">Contact</th>
                                    <th scope="col">Gender</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                <?php while ($row = $result->fetch_assoc()) { ?>
                                    <tr>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td><?php echo $row['contact']; ?></td>
                                        <td><?php echo $row['gender']; ?></td>
                                        <td><?php echo $row['role']; ?></td>
                                        <td>
                                            <a href="#editModal<?php echo $row['user_id']; ?>" data-toggle="modal" data-target="#editModal<?php echo $row['user_id']; ?>">Edit</a>
                                            | 
                                            <a href="?delete=<?php echo $row['user_id']; ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $result->data_seek(0); // Reset the result pointer
    while ($row = $result->fetch_assoc()) {
        echo "<div class='modal fade' id='editModal" . $row['user_id'] . "' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>";
        ?>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Edit user form -->
                    <form action="" method="post">
                        <input type="hidden" name="edit_id" value="<?php echo $row['user_id']; ?>">
                        <div class="form-group">
                            <label for="edit_name">Name</label>
                            <input type="text" class="form-control" id="edit_name" name="name" value="<?php echo $row['name']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="edit_email">Email</label>
                            <input type="email" class="form-control" id="edit_email" name="email" value="<?php echo $row['email']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="edit_contact">Contact</label>
                            <input type="text" class="form-control" id="edit_contact" name="contact" value="<?php echo $row['contact']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="edit_role">Role</label>
                            <input type="text" class="form-control" id="edit_role" name="role" value="<?php echo $row['role']; ?>">
                        </div>
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </form>
                </div>
            </div>
        </div>
        <?php
        echo "</div>";
    }
    ?>

    <!-- Include necessary JS files and scripts -->
</body>
</html>
