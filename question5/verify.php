<?php
session_start();
include "./includes/conn.php"; // Include the database connection

if(isset($_SESSION['email'])){
    ?>
    <script>
        window.location="index.php";
    </script>
    
    <?php
}


if (isset($_POST['Login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    // Sanitize user inputs
    $email = mysqli_real_escape_string($connection, $email);

    // Fetch user from the database
    $query = "SELECT * FROM users WHERE email = '$email' LIMIT 1";
    $result = mysqli_query($connection, $query);

    if ($result && $user = mysqli_fetch_assoc($result)) {
        // Verify the password
        if (password_verify($password, $user['password'])) {
            /*
            $_SESSION['user'] = [
                'id' => $user['id'],
                'email' => $user['email'],
                'role' => $user['role']
            ];
            */
            
            //$_SESSION['user']->email;

            $_SESSION['email'] = $user['email'];

            $role = $user['role'];

            // Define the redirection URLs
            $redirectUrls = [
                'Admin' => 'index.php',
                'Staff' => 'examples/staff.php',
                'User' => 'user_dashboard.php',
            ];

            // Check if the role exists in the redirection array, otherwise use a default URL
            $redirectUrl = isset($redirectUrls[$role]) ? $redirectUrls[$role] : 'default.php';

            header("Location: $redirectUrl"); // Redirect to the appropriate page
            exit; // Important to prevent further execution
        }
    }

    // Invalid login
    header("Location: login.php?error=Invalid email or password");
    exit;
}
?>
