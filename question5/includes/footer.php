<footer class="footer pt-0">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6">
                        <div class="copyright text-center  text-lg-left  text-muted">
                            &copy; 2023 <a href="https://github.com/brianjunior/" class="font-weight-bold ml-1" target="_blank">Okello Brian</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                            <li class="nav-item">
                                <a href="https://twitter.com/Okello_Brian_jr" class="nav-link" target="_blank">Twitter</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.linkedin.com/in/okello-brian-08a3bb21a/" class="nav-link" target="_blank">Linkedin</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.linkedin.com/in/okello-brian-08a3bb21a/" class="nav-link" target="_blank">0781038320</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://github.com/brianjunior/" class="nav-link" target="_blank">Github</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>