<?php
// Define a sample password
$password = "admin123";

// Hash the password
$hashedPassword = password_hash($password, PASSWORD_BCRYPT);

// Output the hashed password
echo "Original Password: $password<br>";
echo "Hashed Password: $hashedPassword";
?>
