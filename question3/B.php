<?php
// Define the file name
$fileName = 'test-file.txt';

// Read the content of the file
$fileContent = file_get_contents($fileName);

// Define an array to store punctuation marks
$punctuationMarks = [];

// Loop through each character in the file content
for ($i = 0; $i < strlen($fileContent); $i++) {
    $char = $fileContent[$i];
    
    // Check if the character is a punctuation mark
    if (preg_match('/[\'",.!?]/', $char)) {
        $punctuationMarks[] = $char;
    }
}

// Print the array of punctuation marks
print_r($punctuationMarks);
?>
