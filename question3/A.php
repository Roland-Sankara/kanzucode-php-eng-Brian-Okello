<?php
function removeDuplicatesFromFile($filename) {
    // Read the content of the file
    $content = file_get_contents($filename);
    
    // Split the content into an array of sentences
    $sentences = explode('.', $content);
    
    // Remove any empty elements and trim spaces
    $sentences = array_map('trim', array_filter($sentences));
    
    // Remove duplicates while preserving the order
    $uniqueSentences = array_unique($sentences);
    
    return $uniqueSentences;
}

// File name
$filename = 'test-file.txt';

// Call the function
$uniqueSentences = removeDuplicatesFromFile($filename);

// Print the unique sentences
foreach ($uniqueSentences as $sentence) {
    echo $sentence . ".\n";
}
?>
