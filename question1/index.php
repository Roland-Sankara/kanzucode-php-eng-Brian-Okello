<?php

// Function to generate Fibonacci numbers
function generateFibonacci($n) {
    $fib = [0, 1];
    for ($i = 2; $i < $n; $i++) {
        $fib[$i] = $fib[$i - 1] + $fib[$i - 2];
    }
    return $fib;
}

//array of numbers from 0 to 100
$numbers = range(0, 100);

// Sort the array in descending order
rsort($numbers);

// Generate Fibonacci numbers starting from the seventh number
$fibonacciNumbers = generateFibonacci(20); // Generating 20 Fibonacci numbers

// Extracting Fibonacci numbers starting from the seventh number
$fibonacciSubset = array_slice($fibonacciNumbers, 6);

print_r($fibonacciSubset);

?>

