<?php

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

// Function to extract email address using regex
function extractEmailAddress($text) {
    $pattern = '/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/';
    preg_match($pattern, $text, $matches);
    return $matches[0] ?? null;
}

// Function to extract phone number using regex
function extractPhoneNumber($text) {
    $pattern = '/\b\d{9,}\b/';
    preg_match($pattern, $text, $matches);
    return $matches[0] ?? null;
}

// Function to extract URL using regex
function extractURL($text) {
    $pattern = '/https?:\/\/[^\s]+/';
    preg_match($pattern, $text, $matches);
    return $matches[0] ?? null;
}

// Test the functions with the provided paragraph
$email = extractEmailAddress($paragraph);
$phoneNumber = extractPhoneNumber($paragraph);
$url = extractURL($paragraph);

echo "Email Address: $email\n";
echo "Phone Number: $phoneNumber\n";
echo "URL: $url\n";

?>
