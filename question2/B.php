<?php
$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

// Function to extract phone number
function extractPhoneNumber($text) {
    $phoneNumber = '';
    $words = explode(" ", $text);
    
    foreach ($words as $word) {
        if (is_numeric($word) && strlen($word) >= 10) {
            $phoneNumber = $word;
            break;
        }
    }
    
    return $phoneNumber;
}

// Function to extract email address
function extractEmailAddress($text) {
    $email = '';
    $words = explode(" ", $text);
    
    foreach ($words as $word) {
        if (filter_var($word, FILTER_VALIDATE_EMAIL)) {
            $email = $word;
            break;
        }
    }
    
    return $email;
}

// Function to extract URL
function extractURL($text) {
    $url = '';
    $words = explode(" ", $text);
    
    foreach ($words as $word) {
        if (filter_var($word, FILTER_VALIDATE_URL)) {
            $url = $word;
            break;
        }
    }
    
    return $url;
}

$phoneNumber = extractPhoneNumber($paragraph);
$emailAddress = extractEmailAddress($paragraph);
$url = extractURL($paragraph);

echo "Phone Number: " . $phoneNumber . "<br>";
echo "Email Address: " . $emailAddress . "<br>";
echo "URL: " . $url . "<br>";
?>
